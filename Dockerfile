# Copyright 2019 Red Hat, Inc.
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.

FROM docker.io/opendevorg/python-builder:3.11-bookworm as builder

COPY . /tmp/src
RUN assemble

FROM docker.io/opendevorg/python-base:3.11-bookworm as zuul-registry

COPY --from=builder /output/ /output
RUN /output/install-from-bindep

VOLUME /storage
CMD ["/usr/local/bin/zuul-registry", "-c", "/conf/registry.yaml", "serve"]
